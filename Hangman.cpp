/*  Hangman Hangman.cpp
    2.05.2015
*/

#include <iostream>
#include <cctype>
#include "Hangman.h"
#include "WordListLoader.h"


// public:
Hangman::Hangman() : word(loadWord())
{
    prepArray();
    gallowsProgress = 0;
    currentRound = 0;

}

void Hangman::welcome()
{
    std::cout   << "\nWelcome to HANGMAN v1\n"
                << "I will print a line of '_' and you are going to guess letters.\n"
                << "If the guessed letter is not part of the answer you will hang.\n"
                << "After 9 failed guesses the game is over.\n"
                << "If your letter is valid it's position in the answer will be reveiled\n"
                << "Have fun.\n"
                << std::endl;
}

void Hangman::getStats()
{
    // std::cout << "Game word:\t" << word <<std::endl;
    std::cout << "Game array:\t" << array <<std::endl;
    std::cout << "Game round:\t" << currentRound <<std::endl;
    std::cout << "Game used:\t" << guesses <<std::endl;
    std::cout << "Game gallow:\t" << gallowsProgress << " of 6" << std::endl;
}

void Hangman::play()
{
    std::cout << "Let's play:" << std::endl;
    try
    {
        while (isDead()==false && isWin()==false)
        {
            // printRound();
            getStats();
            // printArray();
            makeGuess();
            if (evalGuess())
            {
                replaceChar(guess);
            }
            else
            {
                gallowsProgress+=1;
            }
            currentRound+=1;
            std::cout << std::string(50, '\n') << std::endl;
            printGallows(gallowsProgress);
        }
    }

    catch(char const* e)
    {
        std::cout << "! Input-Exception: " << e << std::endl;
    }

    if (isDead()) deadMessage();
    if (isWin()) winMessage();

}


// private:
std::string Hangman::loadWord()
{
    WordListLoader wll("wordlist.txt");
    // std::cout << wll.loadWord() << std::endl;
    return wll.loadWord();
}

void Hangman::prepArray()
{
    for (int i=0; i<word.length(); i++)
    {
        array += "_";
    }
}

void Hangman::printArray()
{
    std::cout << array << std::endl;
}

void Hangman::printRound()
{
    std::cout << "Round " << currentRound << std::endl;
}

void Hangman::printGallows(int in)
{
    switch (in)
    {
        case 0:
            std::cout   << "\t ______\n"
                        << "\t |   |\n"
                        << "\t |   \n"
                        << "\t |  \n"
                        << "\t |  \n"
                        << "\t |\n"
                        << "\t========" << std::endl;
            break;
        case 1:
            std::cout   << "\t ______\n"
                        << "\t |   |\n"
                        << "\t |   O\n"
                        << "\t |  \n"
                        << "\t |  \n"
                        << "\t |\n"
                        << "\t========" << std::endl;
            break;
        case 2:
            std::cout   << "\t ______\n"
                        << "\t |   |\n"
                        << "\t |   O\n"
                        << "\t |   |\n"
                        << "\t |  \n"
                        << "\t |\n"
                        << "\t========" << std::endl;
            break;
        case 3:
            std::cout   << "\t ______\n"
                        << "\t |   |\n"
                        << "\t |   O\n"
                        << "\t |  -|\n"
                        << "\t |  \n"
                        << "\t |\n"
                        << "\t========" << std::endl;
            break;
        case 4:
            std::cout   << "\t ______\n"
                        << "\t |   |\n"
                        << "\t |   O\n"
                        << "\t |  -|-\n"
                        << "\t |  \n"
                        << "\t |\n"
                        << "\t========" << std::endl;
            break;
        case 5:
            std::cout   << "\t ______\n"
                        << "\t |   |\n"
                        << "\t |   O\n"
                        << "\t |  -|-\n"
                        << "\t |  / \n"
                        << "\t |\n"
                        << "\t========" << std::endl;
            break;
        case 6:
            std::cout   << "\t ______\n"
                        << "\t |   |\n"
                        << "\t |   O\n"
                        << "\t |  -|-\n"
                        << "\t |  / \\\n"
                        << "\t |\n"
                        << "\t========" << std::endl;
            break;
        default:
            std::cout   << "\t ______\n"
                        << "\t |   |\n"
                        << "\t |   O\n"
                        << "\t |  -|-\n"
                        << "\t |  / \\\n"
                        << "\t |\n"
                        << "\t========" << std::endl;
    }
}

char Hangman::makeGuess()
{
    std::cout << "Make a guess: ";
    std::cin >> guess;

    if ((guess>=65 && guess<=90) || (guess>=97 && guess<=122)) {
        guesses+=guess;
        guesses+=", ";
        return guess;
    }
    else throw "invalid input";
}

bool Hangman::evalGuess()
{
    if (word.find(guess)!=std::string::npos)
    {
        return true;
    }
    else
    {
        return false;
    }
}

void Hangman::replaceChar(char guess)
{
    for (int i=0; i<word.length(); i++)
    {
        if (word[i] == guess)
        {
            array[i] = guess;
        }
    }
}

bool Hangman::isDead()
{
    if (gallowsProgress<6)
    {
        return false;
    }
    else
    {
        return true;
    }
}

bool Hangman::isWin()
{
    if (array.find('_')==std::string::npos)
    {
        return true;
    }
    else
    {
        return false;
    }
}

void Hangman::deadMessage()
{
    std::cout << "\t" << array << std::endl;
    std::cout << "\t" << word << std::endl;
    std::cout   << "Game Over!\n"
                << "You are a hangin' man.\n"
                << "Try again.\n"
                << std::endl;
}

void Hangman::winMessage()
{
    std::cout << "\t" << array << std::endl;
    // printArray();
    std::cout   << "You win!\n"
                << "You found all the right letters and escaped the gallows\n"
                << "Come back for more ...\n"
                << std::endl;
}