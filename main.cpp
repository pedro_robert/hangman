/*  Hangman main.cpp
    2.05.2015
*/

#include <iostream>
#include "Hangman.h"
#include "WordListLoader.h"

int main()
{
    Hangman h;

    h.welcome();
    h.play();

}