/*  Hangman WordListLoader.cpp
    2.05.2015
*/

#include <iostream>
#include <fstream>
#include <cmath>
#include <ctime>
#include "WordListLoader.h"

//public:
WordListLoader::WordListLoader(char const* file) : fLines(59)
{
    wlist.open(file, std::ifstream::in);

    // std::string str;
    // while (getline(wlist, str))
    // {
    //     fLines++;
    // }

    rnd = rndRange(fLines);
}

WordListLoader::~WordListLoader()
{
    // std::cout << "Deconstructor working ..." << std::endl;
    wlist.close();
}

std::string WordListLoader::loadWord()
{
    std::string line;
    std::string dummy;
    int i=0;
    while(!wlist.eof())
    {
        getline(wlist, dummy);
        if (i==rnd)
        {
            getline(wlist, line);
        }
        i+=1;
    }
    return line;
}

int WordListLoader::getLineCount()
{
    return fLines;
}

int WordListLoader::getRnd()
{
    return rnd;
}

//private:
int WordListLoader::rndRange(int max, int min)
{
    float rnd = (max-min)*(pow( std::sin( (int) std::time(NULL) ), 2) )+min;
    // std::cout << (int) rnd;
    return (int) rnd;
}