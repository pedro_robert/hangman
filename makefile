#
# Hangman Game Makefile
#

CXX=g++
RM=rm -f
CPPFLAGS=-c -g
LDFLAGS=-g
LDLIBS=

##~##
SRCS=Hangman.cpp WordListLoader.cpp main.cpp
OUT_DIR=out
EXE=$(OUT_DIR)/hangman
##~##

OBJS=$(SRCS:.cpp=.o)

# BUILD RECIPE
all: $(SRCS) $(EXE)

$(OUT_DIR):
	mkdir -p $@

$(EXE): $(OBJS) $(OUT_DIR)
	$(CXX) $(LDFLAGS) $(OBJS) -o $@

.cpp.o:
	$(CXX) $(CPPFLAGS) $< -o $@

clean:
	$(RM) $(OBJS)

dist-clean: clean
	$(RM) $(EXE)