/*  Hangman Hangman.h
    2.05.2015
*/

#ifndef HANGMAN_H
#define HANGMAN_H

#include <iostream>

class Hangman
{
public:
    Hangman();
    void welcome();
    void getStats();
    void play();

private:
    std::string loadWord();
    void prepArray();

    void printArray();
    void printRound();
    void printGallows(int in);

    char makeGuess();
    bool evalGuess();
    void replaceChar(char guess);

    bool isDead();
    bool isWin();
    void deadMessage();
    void winMessage();

    const std::string word;
    std::string array;
    std::string guesses;
    char guess;
    int currentRound;
    int gallowsProgress;
};

#endif