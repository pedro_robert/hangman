/*  Hangman WordListLoader.h
    2.05.2015
*/

#ifndef WORDLISTLOADER_H
#define WORDLISTLOADER_H

#include <iostream>
#include <fstream>

class WordListLoader
{
public:
    WordListLoader(char const* file);
    ~WordListLoader();

    std::string loadWord();
    int getLineCount();
    int getRnd();

private:
    std::ifstream wlist;
    /* lines in listfile */
    int fLines;
    int rnd;

    int rndRange(int a, int b = 0);
};

#endif